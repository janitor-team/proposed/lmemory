void lmem_Newgame    (GtkWidget *window, guint callback_action, GtkWidget *widget);
void lmem_Level    (GtkWidget *window, guint callback_action, GtkWidget *widget);
void lmem_Set_Option    (GtkWidget *window, guint callback_action, GtkWidget *widget);
void lmem_rules    (void);
void lmem_option    (void);
void lmem_about    (void);

GtkWidget* create_window_about (void);
GtkWidget* create_window_rules (void);
GtkWidget* create_window_option (void);
