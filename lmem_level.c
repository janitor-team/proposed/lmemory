/* 
 *  Linux memory game
 *  By Xiaoguang Zhang
 */
#include <gtk/gtk.h>
#include <stdlib.h>
#include "lmemory.h"
#include "lmem_menu.h"

void
lmem_from_Little_One ()
{
  gint k,l;

/*
If we are switching from LITTLE_ONE then we need
to reset all the lmem_state so that the cards will be flipped back
*/
    k=0;
    while(k<NUM){
      lmem_state[k] = -lmem_state[k];
      if(lmem_state[k]>0){
        l = lmem_state[k]-match_card;
	default_pixmap[k] = second_pixmap[l];
      }else{
        l = -lmem_state[k]-match_card;
        second_pixmap[l] = default_pixmap[k];
      }
      k++;
    }
}

void
lmem_Level (GtkWidget *window, guint callback_action, GtkWidget * widget)
{
/* switch to Little_One */
  if(callback_action == 2) {
      if(lmem_level == LITTLE_ONE) return;
      lmem_level = LITTLE_ONE;
      lmem_Newgame (window,0,NULL);
      return;
  }
/*
If we are switching from LITTLE_ONE then we need
to reset all the lmem_state so that the cards will be flipped back
*/
  if(lmem_level == LITTLE_ONE) lmem_from_Little_One();
/*
Now change lmem_level
*/
  switch(callback_action) {
    case 3:
      if(lmem_level == BEGINNER) return;
      lmem_level = BEGINNER;
      break;
    case 4:
      if(lmem_level == SKILLED) return;
      lmem_level = SKILLED;
      break;
    case 5:
      if(lmem_level == MASTER) return;
      lmem_level = MASTER;
      break;
    case 6:
      if(lmem_level == DAEMON) return;
      lmem_level = DAEMON;
  }
  lmem_Newgame (window,0,NULL);
}

void
lmem_Set_Option (GtkWidget *window, guint callback_action, GtkWidget * widget)
{
  if(callback_action < 8) {
/*
toggle the value of match_card between 2 and 3
*/
  match_card = 5 - match_card;
  } else {

/*
toggle the value of same_card between 1 and 2
*/
  same_card = 3 - same_card;
  }
  lmem_Newgame (window,0,NULL);
}
