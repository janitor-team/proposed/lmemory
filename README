The memory game
=========================

By Xiaoguang Zhang, <xiaoguangz@home.com>

The Linux Memory Game is an X11 game for children ages 3 and up, based on
the card game "Memory". It is written using GTK+ library. Although it is a
children's game, it has five skill levels, the higher ones are challenging
to adults as well. Additionally, if compiled in the DIFFERENT_CARDS_IN_PAIR
mode, it can be a very good teaching tool for teaching languages, concepts,
or association. This is still an alpha release, but essentially all the
features are already implemented, and the game is very playable.

I hijacked some of the routines from the Linux Letters and Numbers game By
Kirk Ismay, <captain@netidea.com>.  It was built using the GTK (Gimp
ToolKit) for X.  It presently works with 1.2.

For more information about the GTK check out:
 http://www.gtk.org

It is intended for children 3 and up.

Features
======== 

* Extensible - add new images yourself without having to make changes 
  to the program.  

* Five skill levels appropriate for the youngest and the ones with the
  best brains.

* Can play two card matching or three card matching (more challenging!)

* Can set to match different cards. This can be used with apropriately
  designed pictures to teach children (or adults) words, concepts, or
  another language.

Adding Pictures
===============

1. Convert your pictures to xpm format.  Make sure it is no more than
   64x64 pixels for best results.  The Gimp is excellent for this ;-)

2. Copy them to the pixmaps subdirectory (/usr/local/share/pixmaps/ by
   default). You must have at least 8 pictures in the same directory for
   the game to work. You can now put the pixmaps into a different directory
   and point to it through the environment variable LMEM_PIX_DIR.

3. For pictures to be used in the matching different cards mode, you need
   to make two different pictures for each pair. For example, one is a
   smiley face named smile1.xpm, and the second one is the word "smile"
   in the picture and named smile2.xpm. It is important that each pair
   have filenames that use at least the same first few letters because
   that's how the program recognizes that they are in a pair.

4. Start the game and enjoy.
