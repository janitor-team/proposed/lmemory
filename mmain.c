/* 
 *  Linux memory game
 *  By Xiaoguang Zhang
 */

#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include "lmemory.h"

int
main (int argc, char *argv[])
{
  gint x, z, i, j, k;
  char buf[40];
  struct stat statbuf;


  gtk_init (&argc, &argv);

/* read LMEM_PIX_DIR environment variable first before resorting to default
  path for pixmap files --- contributed by philippe.capdepuy@libertysurf.fr */

  lmem_image_dir = getenv("LMEM_PIX_DIR");
  if (stat (lmem_image_dir, &statbuf))
      asprintf (&lmem_image_dir, "%s/pixmaps/", DATADIR);
  if (stat (lmem_image_dir, &statbuf))
      lmem_image_dir = "/usr/share/pixmaps/";
  if (stat (lmem_image_dir, &statbuf))
      lmem_image_dir = "/usr/share/icons/";
  printf ("Using `%s' for image dir\n", lmem_image_dir);

  /* create a new window and setup the pixmaps */
  lmem_setup();

  gtk_main ();

  return 0;
}
