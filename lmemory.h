#define PIXMAP_SIZE 64
#define SIZE 5
#define SIZEP (SIZE+1)
#define NUM SIZE*SIZEP

/* difficulty levels */
#define LITTLE_ONE   0
#define BEGINNER     1
#define SKILLED      2
#define MASTER       3
#define DAEMON       4

const char *indicator[5];
int lmem_level;
int match_card;
int same_card;
int click_count;

/* Window manipulation */

void lmem_flip_card  (GtkWidget *widget, gpointer *data);

void lmem_setup	     ();
void lmem_change_level(GtkWidget *widget, gpointer *data);
void get_main_menu( GtkWidget  *window, GtkWidget **menubar );

/* String Parsing */

GSList *lmem_dir_list (GSList * file_list, gchar * directory_name);

void lln_set_random();
int lln_get_random(double max);

/* pixmaps */
GtkWidget * default_pixmap[NUM];
GtkWidget * second_pixmap[NUM];
GtkWidget * button[NUM];
GtkWidget *frame;
gint lmem_state[NUM];
char *lmem_image_dir;
