lmemory (0.6c-10) unstable; urgency=medium

  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.5.0.
  * Work around FTBFS with GCC 10 by compiling with -fcommon. (Closes: #957497)

 -- Markus Koschany <apo@debian.org>  Wed, 22 Jul 2020 12:45:03 +0200

lmemory (0.6c-9) unstable; urgency=medium

  * Switch to compat level 11.
  * Declare compliance with Debian Policy 4.1.4.
  * Move the package to Git and salsa.debian.org.
  * Change homepage address to https://tracker.debian.org/pkg/lmemory because
    the old one is gone.
  * Remove deprecated menu file.
  * Remove Evgeny Dolgikh from Uploaders.
  * Disable debian/watch because there is nothing to track anymore.

 -- Markus Koschany <apo@debian.org>  Tue, 15 May 2018 14:18:34 +0200

lmemory (0.6c-8) unstable; urgency=medium

  * Add reproducible-build.patch.
    Make the build reproducible by removing the __DATE__ macro and using a
    fixed value instead.
  * Vcs-Browser: Use https.
  * Update my e-mail address.
  * Declare compliance with Debian Policy 3.9.6.

 -- Markus Koschany <apo@debian.org>  Mon, 02 Nov 2015 23:17:31 +0100

lmemory (0.6c-7) unstable; urgency=low

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org.

  [ Markus Koschany ]
  * Add myself to Uploaders.
  * Bump compat level to 9 and require debhelper >= 9.
  * Bump Standards-Version to 3.9.5, no changes.
  * Drop autoconf, automake and libtool from Build-Depends. Use dh-autoreconf
    instead.
  * Build with --parallel and with autoreconf to recreate the build system at
    build time and to avoid a FTBFS. (Closes: #724197)
  * Update dh_auto_clean target for allowing lmemory to build twice in a row.
  * Drop dirs file, not needed.
  * Add keywords to desktop file.
  * Add longtitle to menu file.
  * Drop ac_local_programm.diff, it is not needed.
  * Drop as-needed.patch. Export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed
    instead, so that all LDFLAGS via dpkg-buildflags can be appended.
  * Add DEP3 header to patches.
  * Update debian/copyright to copyright format 1.0.
  * Run wrap-and-sort -sa.

 -- Markus Koschany <apo@gambaru.de>  Fri, 01 Nov 2013 23:26:42 +0100

lmemory (0.6c-6) unstable; urgency=low

  * Team upload.
  * Add --as-needed to linker flags to avoid useless linking.
  * In clean, first generate all build files, because the original source
  has a Makefile, but cannot run make maintainer-clean. (Closes: #634770)

 -- Bas Wijnen <wijnen@debian.org>  Thu, 21 Jul 2011 21:17:06 +0200

lmemory (0.6c-5) unstable; urgency=low

  [ Bas Wijnen ]
  * Team upload.
  * debian/control: remove 'a' from short description
  * Update standards version to 3.9.2 (no changes needed)
  * debian/rules: fix clean target

  [ Evgeny Dolgikh ]
  * Update Standards Version: 3.9.1 (no changes needed)
  * Fix desktop menu entry (Closes: #565510)
    - added resized default.xpm as lmemory.xpm
    - fixed .desktop file
    - fixed menu file
  * Add debian/source/format
  * debian/control
    - add ${misc:Depends}
    - debhelper dep version changed to (>= 7.0.50~)
    - fixed package long description
    - removed quilt depend
  * debian/rules
    - change deprecated dh_clean -k to dh_prep
    - removed apply/unapply patch commands
  * debian/copyright: update GPL-2 text path
  * Add myself in Uploaders
  * debian/changelog: removed trailing colons

 -- Bas Wijnen <wijnen@debian.org>  Tue, 19 Jul 2011 13:37:01 +0200

lmemory (0.6c-4) unstable; urgency=low

  [ Peter De Wachter ]
  * Added watch file.

  [ Gonéri Le Bouder ]
  * Build with gtk+-2.0, Closes: #509961
  * Fix the build system, Closes: #509962
   - call autoreconf, and then add a builddeps on autoconf, automale,
     libtool
   - add ac_local_program.diff to avoid a warning
  * Add a .desktop file
  * use dh_install to install the pixmaps
  * Add myself in Uploaders

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Tue, 17 Feb 2009 00:26:15 +0100

lmemory (0.6c-3) unstable; urgency=low

  [ Miriam Ruiz ]
  * Debian Games Team is taking care of this game from now on
  * Allows for parallel build, Closes: #490333
  * Use Quilt to manage patches
  * Added autotools-dev to Build-Deps

 -- Miriam Ruiz <little_miry@yahoo.es>  Fri, 05 Sep 2008 18:10:01 +0200

lmemory (0.6c-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Applied patch provided by dann frazier <dannf@debian.org> to fix
    implicit-pointer-conversion problem (Thanks to Dann for the patch)
    Closes: #486534

 -- Andreas Tille <tille@debian.org>  Mon, 07 Jul 2008 09:04:31 +0200

lmemory (0.6c-2) unstable; urgency=low

  * Acknowledge NMU.
  * Don't distclean unless we have config.status. (Closes: #476024)

 -- Ben Armstrong <synrg@sanctuary.nslug.ns.ca>  Sun, 15 Jun 2008 10:38:01 -0300

lmemory (0.6c-1.1) unstable; urgency=low

  * Non-maintainer upload to help xlibs-dev transition
  * debian/control: Remove xlibs-dev build-dep, it's not needed. (Closes:
    #346895)

 -- Marc 'HE' Brockschmidt <he@debian.org>  Fri, 20 Jan 2006 18:33:02 +0100

lmemory (0.6c-1) unstable; urgency=low

  * New upstream.
  * Freshen config.sub, config.guess. (Closes: #240165)

 -- Ben Armstrong <synrg@fountain.nslug.ns.ca>  Fri, 26 Mar 2004 09:54:52 -0400

lmemory (0.5-7) unstable; urgency=medium

  * Fix #143893 for real this time.  The first fix worked for SKILLED
    mode only.  But DAEMON mode still was prone to seg fault.  Thanks
    to 'jannic' and others on #debian-devel for help with this one.

 -- Ben Armstrong <synrg@sanctuary.nslug.ns.ca>  Mon, 22 Apr 2002 10:34:51 -0300

lmemory (0.5-6) unstable; urgency=low

  * Fix seg fault.  (Closes: #143893)

 -- Ben Armstrong <synrg@sanctuary.nslug.ns.ca>  Mon, 22 Apr 2002 07:54:02 -0300

lmemory (0.5-5) unstable; urgency=low

  * Remove -Werror as it prevents build on alpha, and I have
    been unable to resolve this last warning for that port.
    Besides, the warning is rather unlikely to cause runtime
    errors.  (Closes: #137596)

 -- Ben Armstrong <synrg@sanctuary.nslug.ns.ca>  Sat, 16 Mar 2002 15:39:40 -0400

lmemory (0.5-4) unstable; urgency=low

  * Knock off last compiler warning.

 -- Ben Armstrong <synrg@sanctuary.nslug.ns.ca>  Mon, 31 Dec 2001 10:53:31 -0400

lmemory (0.5-3) unstable; urgency=low

  * The "eliminate warnings in non-i386 arches release".

 -- Ben Armstrong <synrg@fountain.nslug.ns.ca>  Thu, 27 Dec 2001 20:21:52 -0400

lmemory (0.5-2) unstable; urgency=low

  * Updated config.guess, config.sub from /usr/share/misc.

 -- Ben Armstrong <synrg@fountain.nslug.ns.ca>  Sun, 23 Dec 2001 12:18:51 -0400

lmemory (0.5-1) unstable; urgency=low

  * New upstream.  (Closes: #125876)

 -- Ben Armstrong <synrg@fountain.nslug.ns.ca>  Sat, 22 Dec 2001 11:39:04 -0400

lmemory (0.3-3) unstable; urgency=low

  * Clean up packaging.

 -- Ben Armstrong <synrg@fountain.nslug.ns.ca>  Tue, 18 Dec 2001 15:09:39 -0400

lmemory (0.3-2) unstable; urgency=low

  * New config.guess and config.sub.  (Closes: #97384)

 -- Ben Armstrong <synrg@sanctuary.nslug.ns.ca>  Mon, 14 May 2001 09:12:57 -0300

lmemory (0.3-1) unstable; urgency=low

  * New upstream release.  Now uses all images.

 -- Ben Armstrong <synrg@sanctuary.nslug.ns.ca>  Mon, 20 Nov 2000 14:06:07 -0400

lmemory (0.2b-2) unstable; urgency=low

  * Added missing build-depends for libgtk1.2-dev, xlibs-dev (Closes:#77518)

 -- Ben Armstrong <synrg@sanctuary.nslug.ns.ca>  Mon, 20 Nov 2000 11:19:50 -0400

lmemory (0.2b-1) unstable; urgency=low

  * Initial Release.

 -- Ben Armstrong <synrg@sanctuary.nslug.ns.ca>  Sat, 11 Nov 2000 13:14:25 -0400
